<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoFrame extends Model
{
    use HasFactory;

    protected $table = "tipo_frame";

}
