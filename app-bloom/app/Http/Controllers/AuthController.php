<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Validator;
use Auth;
use Log;

class AuthController extends Controller
{
    //
    public function authenticate(Request $request){

      try {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'message' => $validator->messages(), 'success' => false ], 400);
        }

        $res = Auth::attempt([ 'email' => $request['email'], 'password' => $request['password'] ]);

        if ($res) {
          $user = Auth::user();
          //$token = $this->updateToken($user);
          // $response['token'] =  $token;
          $response['token'] =  "Prueba de token";
          $response['success'] = true;
          $response['message'] = "Logeado exitosamente";
        }
        else {
          $response['success'] = false;
          $response['message'] = "Contraseña o Email es incorrecto";
        }
        return $response;
      }
      catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
    }

    public function updateToken($user)
    {
      $token = Str::random(60);
      $user = User::find($user->id);
      $newToken = hash('sha256', $token);

      $user->forceFill([
          'api_token' => $newToken,
      ])->update();

      return $newToken;
    }

    public function create($request)
    {
      $data['name'] = $request['name'];
      $data['email'] = $request['email'];
      $data['password'] = $request['password'];

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'api_token' => Str::random(60),
        ]);
    }

    public function test(Request $request)
    {
      $data['name'] = "Felipe Perdomo";
      $data['email'] = "prueba@mail.com";
      $data['password'] = "123456";

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'api_token' => Str::random(60),
        ]);
    }
}
