<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\Producto;
Use App\Models\TipoFrame;
Use App\Models\TipoLente;

class HomeController extends Controller
{
    //
    public function main(){

      try {
        $response['tipo_frame'] = TipoFrame::where("estado",1)->get();
        $response['tipo_lente'] = TipoLente::where("estado",1)->get();
        $response['productos'] = Producto::where("estado",1)->get();
        $response['message'] = "Cargo exitosamente.";
        $response['success'] = true;

      } catch (\Exception $e) {
        $response['success'] = true;
        $response['message'] = $e->getMessage();
      }
       return $response;
    }
}
