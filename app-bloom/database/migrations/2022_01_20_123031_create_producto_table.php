<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('productos', function (Blueprint $table) {
        $table->id();
        $table->string('nombre',100);
        $table->string('descripcion',500);
        $table->integer('precio');
        $table->integer('stock');
        $table->text('imagen');
        $table->unsignedBigInteger('tipo_lente_id');
        $table->foreign('tipo_lente_id')->references('id')->on('tipo_lente');
        $table->unsignedBigInteger('tipo_frame_id');
        $table->foreign('tipo_frame_id')->references('id')->on('tipo_frame');
        $table->boolean('estado');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto');
    }
}
