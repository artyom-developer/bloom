<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
Use App\Models\TipoLente;

class TipoLenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // TipoLente::delete();

        $elementosListas = [
         [
           'id'=> 1,
           'nombre' => "Acetato",
           'estado' => 1
         ],
         [
           'id'=> 2,
           'nombre' => "Poliuretano",
           'estado' => 1
         ],
         [
           'id'=> 3,
           'nombre' => "Acero",
           'estado' => 1
         ]
       ];

       TipoLente::insert($elementosListas);
    }
}
