<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
Use App\Models\TipoFrame;

class TipoFrameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 

        $elementosListas = [
         [
           'id'=> 1,
           'nombre' => "Degradado",
           'estado' => 1
         ],
         [
           'id'=> 2,
           'nombre' => "Traslúsido",
           'estado' => 1
         ],
         [
           'id'=> 3,
           'nombre' => "Iridiscente",
           'estado' => 1
         ]
       ];

       TipoFrame::insert($elementosListas);
    }
}
