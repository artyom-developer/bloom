<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
Use App\Models\Producto;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        //
        $elementosListas = [
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 54000,
           'stock' => 4,
           'tipo_lente_id' => 1,
           'tipo_frame_id' => 1,
           'imagen' => "1G.jpg",
           'estado' => 1
         ],
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 42000,
           'stock' => 3,
           'tipo_lente_id' => 3,
           'tipo_frame_id' => 2,
           'imagen' => "2G.jpg",
           'estado' => 1
         ],
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 34000,
           'stock' => 1,
           'tipo_lente_id' => 1,
           'tipo_frame_id' => 3,
           'imagen' => "3G.jpg",
           'estado' => 1
         ],
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 16000,
           'stock' => 4,
           'tipo_lente_id' => 3,
           'tipo_frame_id' => 2,
           'imagen' => "4G.jpg",
           'estado' => 1
         ],
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 54000,
           'stock' => 4,
           'tipo_lente_id' => 1,
           'tipo_frame_id' => 3,
           'imagen' => "5G.jpg",
           'estado' => 1
         ],
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 63500,
           'stock' => 8,
           'tipo_lente_id' => 2,
           'tipo_frame_id' => 1,
           'imagen' => "6G.jpg",
           'estado' => 1
         ],
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 47000,
           'stock' => 8,
           'tipo_lente_id' => 3,
           'tipo_frame_id' => 1,
           'imagen' => "7G.jpg",
           'estado' => 1
         ],
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 54000,
           'stock' => 4,
           'tipo_lente_id' => 2,
           'tipo_frame_id' => 1,
           'imagen' => "8G.jpg",
           'estado' => 1
         ],
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 54000,
           'stock' => 4,
           'tipo_lente_id' => 2,
           'tipo_frame_id' => 2,
           'imagen' => "9G.jpg",
           'estado' => 1
         ],
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 54000,
           'stock' => 4,
           'tipo_lente_id' => 3,
           'tipo_frame_id' => 3,
           'imagen' => "10G.jpg",
           'estado' => 1
         ],
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 54000,
           'stock' => 4,
           'tipo_lente_id' => 1,
           'tipo_frame_id' => 2,
           'imagen' => "11G.jpg",
           'estado' => 1
         ],
         [
           'nombre' => "Gafas",
           'descripcion' => '...',
           'precio' => 32000,
           'stock' => 4,
           'tipo_lente_id' => 1,
           'tipo_frame_id' => 2,
           'imagen' => "12G.jpg",
           'estado' => 1
         ]
       ];

       Producto::insert($elementosListas);

    }
}
