import React, { useEffect, useState  } from 'react';
import ReactDOM from 'react-dom';
import '../style/general.css';

import api from "../services/axios"

function Login() {

    const [ error, setError ] = useState(null);
    const [ email, setEmail ] = useState(null);
    const [ password, setPassword ] = useState(null);

    useEffect(()=>{
      async function fetchDataToken(){
        const res = await localStorage.getItem('myToken');
      }
      fetchDataToken();
    },[]);

    const onClickLogin = async () => {

        const data = { email, password }

        const res = await api.post('auth',data);

        if (res.success&&res.token) {
          localStorage.setItem('myToken', res.token);
          // alert(JSON.stringify(res.token))
          window.location.replace("/");
        }
        if (!res.success) {
          setError(res.message)
        }

        console.log("Resultado de api");
        console.log(res);

      }

    return (
      <div style={{margin:'auto', width:400, textAlign:'center' }}>
        <main class="form-signin shadowbox" style={{padding:50, backgroundColor:'white', marginTop: 30, borderRadius: 14}}>

            <img class="mb-4" src="/logo.png" alt="" width="70%"  />
            <div style={{height:25 }} />
            <h1 class="h3 mb-3 ff-nexabd tx-20  tx-color">Ingrese a su cuenta</h1>
            <div class="form-floating p-r" >
              <img src="/img/user.png" class="icon-input" alt="" width="20" height="20" />
              <input type="email" class="form-control input-login" id="floatingInput" placeholder="name@example.com"
                value={email} onChange={(event)=>setEmail(event.target.value)}/>
            </div>
            <div class="form-floating p-r" >
              <img src="/img/lock.png" class="icon-input" alt="" width="20" height="20" />
              <input type="password" class="form-control input-login" id="floatingPassword" placeholder="Password"
                value={password} onChange={(event)=>setPassword(event.target.value)} />
            </div>
            <div style={{height:15 }} />
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div style={{display:'flex', flexDirection:'row'}}>
                <img src="/img/casilla.png" alt="" width="10" height="10" />
                <div style={{width:6}} />
                <label class="ff-nexabk tx-color tx-11" > No cerrar sesión </label>
              </div>
              <label class="ff-nexabk tx-color tx-11"> Olvidé mi contraseña</label>
            </div>

            {
              error&&
              <label class="ff-nexabk tx-color tx-11" style={{color:'red'}}>{error}</label>
            }

            <div style={{height:50 }} />
            <button class="w-100 ff-nexabd btn btn-lg btn-primary c-w"
              onClick={()=>onClickLogin()} type="submit">Iniciar sesión</button>
 
          </main>
      </div>
    );
}

export default Login;

if (document.getElementById('login')) {
    ReactDOM.render(<Login />, document.getElementById('login'));
}
