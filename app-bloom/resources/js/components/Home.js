import React, { useEffect, useState  } from 'react';
import ReactDOM from 'react-dom';
import '../style/general.css';

import api from "../services/axios"

function Home() {

    const [ listTipoLente, setListTipoLente ] = useState([]);
    const [ listTipoFrame, setListTipoFrame ] = useState([]);
    const [ listProducto, setListProducto ] = useState([]);
    const [ listProducto2, setListProducto2 ] = useState([]);
    const [ checkTipoLente, setCheckTipoLente] = useState([]);
    const [ checkTipoFrame, setCheckTipoFrame] = useState([]);
    const [ textFrame , setTextFrame ] = useState(1);

    // carrito
    const [ listCart, setListcart ] = useState([])
    const [ showCart, setShowCart ] = useState(false)

    useEffect(()=>{
      loadData();
    },[]);

    const loadData = async () => {

      const res = await api.get('main');

      console.log(res)

      if (res.success) {
        setListTipoFrame(res.tipo_frame);
        setListTipoLente(res.tipo_lente);
        setListProducto(res.productos);
        setListProducto2(res.productos);
      }

    }

    const setTextNumber = () => {
      setTextFrame((textFrame+1))
    }

    const onClickCheckTipoLente = (id) => {
      // alert("Hola "+id)
      const data = checkTipoLente
      data.push(id)
      setCheckTipoLente(data)
      setTextNumber()
    }

    const onClickUnCheckTipoLente = (id) => {
      // alert("Que "+id)
        const findIndex = checkTipoLente.findIndex((item)=>item==id)
        const data = checkTipoLente
        data.splice(findIndex,1)
        setCheckTipoLente(data)
        setTextNumber()
    }

    const hasTipoLente = (id) => {
      const res = checkTipoLente.filter((item)=>item==id)
      console.log(id+" Resultado tiene "+JSON.stringify(res));
      if (res.length>=1) {
        return true;
      }
      else {
        return false;
      }
    }

    const onClickCheckTipoFrame = (id) => {
      // alert("Hola "+id)
      const data = checkTipoFrame
      data.push(id)
      setCheckTipoFrame(data)
      setTextNumber()
    }

    const onClickUnCheckTipoFrame = (id) => {
      // alert("Que "+id)
        const findIndex = checkTipoFrame.findIndex((item)=>item==id)
        const data = checkTipoFrame
        data.splice(findIndex,1)
        setCheckTipoFrame(data)
        setTextNumber()
    }

    const hasTipoFrame = (id) => {
      const res = checkTipoFrame.filter((item)=>item==id)
      console.log(id+" Resultado tiene "+JSON.stringify(res));
      if (res.length>=1) {
        return true;
      }
      else {
        return false;
      }
    }

    const filterProduct = (data) => {
      var valid = true
      // si hay seleccion de tipo frame
      if(checkTipoFrame.length>=1){
        const res = checkTipoFrame.filter((item)=>item==data.tipo_frame_id)
        if(res.length==0){
          return false;
        }
      }
      if(checkTipoLente.length>=1){
        const res = checkTipoLente.filter((item)=>item==data.tipo_lente_id)
        if(res.length==0){
          return false;
        }
      }
      return valid

    }

    const convertMoney = (value) => {
      const formatterPeso = new Intl.NumberFormat('es-CO', {
         style: 'currency',
         currency: 'COP',
         minimumFractionDigits: 0
      })
     return formatterPeso.format(value)
     // → $ 12.500
    }

    const onClickAdd = (item) =>{
      let cart = listCart
      cart.push({
        id: item.id,
        data: item,
        cantidad: 1
      })
      setListcart(cart)
      setTextNumber()
    }

    const onLoadCartNumber = (item) =>{
      let cantidad = 0
      listCart.map((item)=>{
         cantidad = cantidad + item.cantidad
      })
      return cantidad
    }

    const deleteItem = (id) =>{
      const findIndex = listCart.findIndex((item)=>item.id==id)
      const data = listCart
      data.splice(findIndex,1)
      setListcart(data)
      setTextNumber()
    }

    const getTotalCart = () => {
      let cantidad = 0
      listCart.map((item)=>{
         cantidad = cantidad + (item.cantidad*item.data.precio)
      })
      return cantidad
    }

    return (
      <main>
        <div >
          <header class="  header-home" >
            <div class="logo-header">
              <img src="/logo-n.png" alt="" width="100px"  />
            </div>
            <div class="boxcart">

              <a href="/#" onClick={()=>setShowCart(!showCart)}>
                {
                  onLoadCartNumber()>=1?
                  <span class="ff-nexab num-cart">{onLoadCartNumber()}</span>
                  :null
                }
                <img class="icon-header" src="/img/cart.png" alt="" width="30" height="30" />
              </a>

              <a href="/login">
                <img class="icon-header" src="/img/user-cart.png" alt="" width="30" height="30" />
              </a>
            </div>
            {
              showCart&&
              <div class="boxcart-shop shadowbox">
                <h4 class="ff-nexabd tx-color" style={{padding:10,textAlign:'center',paddingBottom:5}}>
                  Bolsa de compras
                </h4>
                <hr/>
                {
                  listCart.length==0?
                  <h3 class="ff-nexab">No hay productos</h3>
                  :listCart.map((item)=>{
                    return(

                      <div style={{display:'flex'}}>
                        <img src={"/img/productos/"+item.data.imagen} width="100px" />
                        <div style={{marginTop:6, marginLeft:10}}>
                          <h5 class="card-title ff-nexabd m0">{item.data.nombre}</h5>
                          <p class="card-text ff-nexabk m0">{item.cantidad}x{convertMoney(item.data.precio)}</p>
                          <a href="#" onClick={()=>deleteItem(item.id)}>
                            <img  src="/img/delete.png" width="26" height="26" />
                          </a>
                        </div>
                      </div>


                    )
                  })
                }
                <hr/>
                  <h4 class="ff-nexabk tx-color"  >
                    Total: {convertMoney(getTotalCart())}
                  </h4>

                {
                  listCart.length>=1?
                  <button class="w-100 ff-nexabd btn btn-lg btn-primary c-w" type="submit">Finalizar compra</button>
                  :null
                }
              </div>
            }


          </header>

          <section class="container section-home" >
            <div class="card margin-vert-20" >
              <div class="card-body bk-primary shadowbox" style={{height:88, borderRadius:10}}>
                <h4 class="card-title ff-nexabd c-w m0">Tienda  <span style={{color:'transparent'}}>{textFrame}</span></h4>
                <div class="template-demo">
                  <nav aria-label="breadcrumb " >
                    <ol class="breadcrumb breadcrumb-custom" style={{backgroundColor:'transparent', padding:0}}>
                      <li class="breadcrumb-item ff-nexabk c-w"><a href="#" class="c-w">Inicio</a></li>
                      <li class="breadcrumb-item active ff-nexabk c-w" aria-current="page"><span class="c-w">Tienda</span></li>
                    </ol>
                  </nav>
                </div>
              </div>
            </div>


            <img  src="/banner.png" alt="" width="100%"  />


            <div class="row align-items-md-stretch">
              <div class="col-md-3">
                <div class="sidebar shadowbox">

                  <h5 class="ff-nexabd">Filtrar productos</h5>

                  <h6 class="ff-nexabd" >Tipo de lente: </h6>
                  {
                    listTipoLente.map((item)=>{
                      return(
                        <div>
                          {
                            hasTipoLente(item.id)?
                            <img class="icon-header" src="/img/checkbox.png" width="23" height="23"
                              onClick={()=>onClickUnCheckTipoLente(item.id)} />
                            :
                            <img class="icon-header" src="/img/casilla.png" width="20" height="20"
                              onClick={()=>onClickCheckTipoLente(item.id)}  />
                          }
                          <label>{item.nombre}</label>
                        </div>
                      )
                    })
                  }

                  <br />
                  <h6 class="ff-nexabd" >Tipo de Frame: </h6>
                  {
                    listTipoFrame.map((item)=>{
                      return(
                        <div>
                          {
                            hasTipoFrame(item.id)?
                            <img class="icon-header" src="/img/checkbox.png" width="23" height="23"
                              onClick={()=>onClickUnCheckTipoFrame(item.id)} />
                            :
                            <img class="icon-header" src="/img/casilla.png" width="20" height="20"
                              onClick={()=>onClickCheckTipoFrame(item.id)}  />
                          }
                          <label>{item.nombre}</label>
                        </div>
                      )
                    })
                  }

                </div>

              </div>
              <div class="col-md-9 boxproduct">

                {
                  listProducto.map((item)=>{
                    if(filterProduct(item)){
                      return(
                        <div class="container-card">
                          <div class="card-prod">
                            <div class="imgBx">
                              <img src={"/img/productos/"+item.imagen}/>
                            </div>
                            <div class="contentBx">
                              <h3 class="ff-nexab tx-color ff-nexab">{item.nombre}</h3>
                              <div class="price-prod">
                                <h4 class="ff-nexabd tx-color" style={{fontSize:20}}>
                                  {convertMoney(item.precio)}
                                </h4>
                              </div>

                              <button class="w-100 ff-nexabd btn btn-lg btn-primary c-w" style={{backgroundColor:"#ff832a"}}
                                onClick={()=>onClickAdd(item)} type="submit">Agregar</button>

                            </div>
                          </div>
                        </div>
                      )
                    }
                  })
                }


              </div>
            </div>
          </section>


          <footer class="pt-3 mt-4 text-muted border-top">
            V2.0.1 © 2021, Desarrollado por Loatech®
          </footer>
        </div>
      </main>

    );
}

export default Home;

if (document.getElementById('home')) {
    ReactDOM.render(<Home />, document.getElementById('home'));
}
